module pm_parameters

  logical,parameter :: part_memory=.true. ! Optimize particle memory distribution

  integer,parameter :: action_kick_only = 1
  integer,parameter :: action_kick_drift = 2  

end module pm_parameters
